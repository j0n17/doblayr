/*
** main.c for main in /home/maire_j/Desktop/Test
**
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
**
** Started on  Wed Oct 30 08:57:19 2013 Maire Jonathan
** Last update Wed Oct 30 22:31:50 2013 Maire Jonathan
*/

#include "libmy/my.h"
#include <dirent.h>
#include <stdlib.h>
#include "myls.h"

int	test_dir(char *dir)
{
  DIR   *dirp;

  if ((dirp = opendir(dir)) == 0)
    {
      my_putstr("No such file or directory");
      return (1);
    }
  closedir(dirp);
  open_dir(dir);
  return(0);
}

void	open_dir(char *dir)
{
  DIR	*dirp;
  char	**dirtab;
  int	count;
  struct dirent	*d;

  count = 0;
  dirtab = malloc(count + 1);
  if (dirtab != 0)
    {
      dirp = opendir(dir);
      while ((d = readdir(dirp)) != 0)
	{
	  my_putstr(d->d_name);
	  my_putchar('\t');
	  count = count + 1;
	}
      closedir(dirp);
      dirtab[count] = my_strdup("\0");
    }
}

int	main(int ac, char **av)
{
  if (ac == 2)
    {
      test_dir(av[1]);
    }
  my_putchar('\n');
  return (0);
}
