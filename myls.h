/*
** myls.h for myls in /home/maire_j/Desktop/Test
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct 30 22:26:19 2013 Maire Jonathan
** Last update Wed Oct 30 22:27:35 2013 Maire Jonathan
*/

#ifndef MYLS_H_
# define MYLS_H_

void	open_dir(char *);
int	test_dir(char *);

#endif /* MYLS_H_ */
