##
## Makefile for Makefile in /home/maire_j/rendu/myls-2013-maire_j
## 
## Made by Maire Jonathan
## Login   <maire_j@epitech.net>
## 
## Started on  Mon Oct 28 10:27:34 2013 Maire Jonathan
## Last update Wed Oct 30 21:48:10 2013 Maire Jonathan
##

CC	= gcc -Wall

RM	= rm -f

NAME	= my_ls

SRCS	= main.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) -lmy -Llibmy/
	$(RM) $(OBJS)
clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all
