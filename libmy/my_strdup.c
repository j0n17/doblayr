/*
** my_strdup.c for my_strdup.c in /home/maire_j/rendu/Piscine-C-Jour_08
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  9 09:20:35 2013 Maire Jonathan
** Last update Wed Oct 30 11:05:13 2013 Maire Jonathan
*/
#include <stdlib.h>

char	*my_strdup(char *src)
{
  char	*dest;

  dest = malloc(my_strlen(src) + 1);
  if (dest != NULL)
    {
      my_strcpy(dest, src);
    }
  else
    {
      my_putstr("Allocation memory error !");
    }
  return (dest);
}
