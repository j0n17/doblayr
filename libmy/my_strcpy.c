/*
** my_strcpy.c for lib in /home/maire_j/Desktop/Programmation_C/fakeC
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 20:17:18 2013 Maire Jonathan
** Last update Thu Oct 24 13:59:47 2013 Maire Jonathan
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}
