/*
** my_strncmp.c for lib in /home/maire_j/Desktop/Programmation_C/fakeC
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 20:20:03 2013 Maire Jonathan
** Last update Tue Oct 29 15:36:34 2013 Maire Jonathan
*/

int	my_strncmp(char *s1, char *s2, int nb)
{
  int	i;

  i = 0;
  while (s1[i] && s2[i] && i + 1 <= nb)
    {
      if (s1[i] != s2[i])
	{
	  return (-1);
	}
      i = i + 1;
    }
  return (0); 
}
